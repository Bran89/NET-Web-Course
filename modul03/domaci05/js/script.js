$(document).ready(function(){

	//Cuvamo podatke od interesa u promenljivim
	var api_key = 'appid=3726fe9cef4ab9d506ee77d8d8f7f3d1';
	var rest_url = "http://api.openweathermap.org/data/2.5/weather?";
	var format = "format=json";


	$("#btnName").click(function(){
		var method = "q=";
		var find_name = $("#tbInput").val(); //vrednost input polja je email
		var requestUrl = rest_url+method+find_name+"&"+api_key;

		console.log(requestUrl);
		
		$.get(requestUrl, postaviGrad,"json");
	});
	
	$("#btnZip").click(function(){
		var method = "zip=";
		var find_zip = $("#tbInput").val(); //vrednost input polja je email
		var requestUrl = rest_url+method+find_zip+"&"+api_key;

		console.log(requestUrl);

		$.get(requestUrl, postaviGrad,"json");
	});
	
	$("#btnId").click(function(){
		var method = "id=";
		var find_id = $("#tbInput").val(); //vrednost input polja je email
		var requestUrl = rest_url+method+find_id+"&"+api_key;

		console.log(requestUrl);
		
		$.get(requestUrl, postaviGrad,"json");
	});


	function postaviGrad(data, status){
		//Dobili smo odgovor od servera, dodajemo podatke u DOM
		var $container = $("#data");
		$container.empty(); //Ispraznimo prethodni sadrzaj, stavljamo podatke o novom korisniku
		if(data.cod == 200 && status == "success"){ //Proveravamo odgovor od servera, da li je zahtev uspesno obradjen
			
			var divId = $("<div></div>"); //Dinamicki pravimo elemente i dodajemo u DOM
			var h1Id = $("<h1></h1>");
			h1Id.text("Id: " + data.id);
			h1Id.attr("id","cityId");
			divId.append(h1Id);
			var divName = $("<div></div>");
			var h1Name = $("<h1></h1>");
			h1Name.text("City: " + data.name);
			h1Name.attr("id","cityName");
			divName.append(h1Name);
			var divCountry = $("<div></div>");
			var h1Country = $("<h1></h1>");
			h1Country.text("Country: " + data.sys.country);
			h1Country.attr("id","cityCountry");
			divCountry.append(h1Country);
			
			$container.append(divId);
			$container.append(divName);
			$container.append(divCountry);
			$container.append("<br/>Current temperature: " + data.main.temp + " K");
			$container.append("<br/>Min temperature: " + data.main.temp_min + " K");
			$container.append("<br/>Max temperature: " + data.main.temp_max + " K");
			$container.append("<br/>Humidity: " + data.main.humidity + "%");
			$container.append("<br/>Pressure: " + data.main.pressure + " Pa");
		}else {
			var div = $("<div></div>"); //Ako nije zahtev prosao uspesno, obavestavamo korisnika o tome
			var h1 = $("<h1>Couldn't find: "+ $("#tbInput").val() +"</h1>");
			div.append(h1);
			$container.append(div);
			$container.append(data.message);
		};
	};

});