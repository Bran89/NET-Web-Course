﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Dealership.Models
{
    public class Manufacturer
    {
        public virtual int Id { get; set; }
        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual string Country { get; set; }
        [Required]
        public virtual string City { get; set; }
        public virtual ICollection<Dealership> Dealerships { get; set; }
    }
}