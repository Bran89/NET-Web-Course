﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Dealership.Models
{
    public class DealershipContext : DbContext
    {
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Dealership> Dealerships { get; set; }

        public DealershipContext(): base("name=VersusDbContext")
        {

        }
    }
}