﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dealership.Models
{
    public class Dealership
    {
        public virtual int Id { get; set; }
        public virtual int Pib { get; set; }
        public virtual string Name { get; set; }
        public virtual string Country { get; set; }
        public virtual string City { get; set; }
        public virtual string Address { get; set; }

        public virtual ICollection<Manufacturer> Manufacturers { get; set; }
    }
}