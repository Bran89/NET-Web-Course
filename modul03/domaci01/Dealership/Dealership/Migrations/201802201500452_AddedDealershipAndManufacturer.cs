namespace Dealership.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDealershipAndManufacturer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dealerships",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Pib = c.Int(nullable: false),
                        Name = c.String(),
                        Country = c.String(),
                        City = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Manufacturers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Country = c.String(nullable: false),
                        City = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ManufacturerDealerships",
                c => new
                    {
                        Manufacturer_Id = c.Int(nullable: false),
                        Dealership_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Manufacturer_Id, t.Dealership_Id })
                .ForeignKey("dbo.Manufacturers", t => t.Manufacturer_Id, cascadeDelete: true)
                .ForeignKey("dbo.Dealerships", t => t.Dealership_Id, cascadeDelete: true)
                .Index(t => t.Manufacturer_Id)
                .Index(t => t.Dealership_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ManufacturerDealerships", "Dealership_Id", "dbo.Dealerships");
            DropForeignKey("dbo.ManufacturerDealerships", "Manufacturer_Id", "dbo.Manufacturers");
            DropIndex("dbo.ManufacturerDealerships", new[] { "Dealership_Id" });
            DropIndex("dbo.ManufacturerDealerships", new[] { "Manufacturer_Id" });
            DropTable("dbo.ManufacturerDealerships");
            DropTable("dbo.Manufacturers");
            DropTable("dbo.Dealerships");
        }
    }
}
