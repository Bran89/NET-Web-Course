﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library_Razor.Models
{
    public class Book
    {
        public static int Brojac = 0;
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public Genre Genre { get; set; }
        public bool Deleted { get; set; }

        public Book()
        {
            this.Deleted = false;
        }

        public void Add(string name, int price, Genre genre)
        {
            this.Name = name;
            this.Price = price;
            this.Genre = genre;
            this.Deleted = false;
        }
    }
}