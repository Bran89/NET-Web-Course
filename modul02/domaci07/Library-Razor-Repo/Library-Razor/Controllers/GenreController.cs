﻿using Library_Razor.Models;
using Library_Razor.Repository;
using Library_Razor.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library_Razor.Controllers
{
    [RoutePrefix("Zanr")]
    public class GenreController : Controller
    {
        private IGenreRepository genreRepo = new GenreRepo();
        
        [Route("")]
        public ActionResult Index()
        {
            var genres = genreRepo.GetAll();
            return View(genres);
        }

        [Route("napravi")]
        public ActionResult Create()
        {
            return View();
        }

        [Route("napravi")]
        [HttpPost]
        public ActionResult Create(Genre genre)
        {
            if (genreRepo.Create(genre))
            {
                return RedirectToAction("Index");
            }
            return View("Error");
        }

        [Route("izbrisi")]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                genreRepo.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        [Route("izmena")]
        public ActionResult Edit(int id)
        {
            var genre = genreRepo.GetById(id);

            return View(genre);
        }

        [Route("izmena")]
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                int exisingId = int.Parse(collection["Id"]);
                string newName = collection["Name"];

                var updatedGenre = new Genre()
                {
                    Id = exisingId,
                    Name = newName,
                };

                genreRepo.Update(updatedGenre);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}