﻿using Library_Razor.Models;
using Library_Razor.Repository;
using Library_Razor.Repository.Interfaces;
using Library_Razor.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library_Razor.Controllers
{
    [RoutePrefix("Knjige")]
    public class BookController : Controller
    {
        private IBookRepository bookRepo = new BookRepo();
        private IGenreRepository genreRepo = new GenreRepo();
        
        [Route("")]
        public ActionResult Index()
        {
            var books = bookRepo.GetAll();
            return View(books);
        }

        // TODO : doraditi, namestiti u cshtml opciju, prebaciti u repo?
        [Route("sortiraj")]
        public ActionResult Sort(int id)
        {
            var books = bookRepo.GetAll();
            List<Book> SortedList = new List<Book>();
            if (id == 1)
            {
                SortedList = books.OrderBy(o => o.Name).ToList();
            }
            else if (id == 2)
            {
                SortedList = books.OrderByDescending(o => o.Name).ToList();
            }
            else if (id == 3)
            {
                SortedList = books.OrderBy(o => o.Price).ToList();
            }
            else if (id == 4)
            {
                SortedList = books.OrderByDescending(o => o.Price).ToList();
            }

            return View("Index", SortedList);
        }

        [Route("napravi")]
        public ActionResult Create()
        {
            BookGenreViewModel bookGenres = new BookGenreViewModel();
            bookGenres.Genres = genreRepo.GetAll().ToList();
            return View(bookGenres);
        }

        [Route("napravi")]
        [HttpPost]
        public ActionResult Create(BookGenreViewModel bookGenres)
        {
            if (bookRepo.Exist(bookGenres.Book.Name))
            {
                return Content("Knjiga sa zadatim nazivom vec postoji!");
            }
            else if (bookRepo.Create(bookGenres))
            {
                return RedirectToAction("Index");
            }
            return View("Error");
        }

        [Route("izbrisi")]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                bookRepo.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        [Route("pretraga")]
        public ActionResult Find()
        {
            return View();
        }

        [Route("pretraga")]
        [HttpPost]
        public ActionResult Find(string name)
        {
            var books = bookRepo.FindByTitle(name);
            return View("Index", books);
        }

        [Route("izmena")]
        public ActionResult Edit(int id)
        {
            BookGenreViewModel bookGenres = new BookGenreViewModel();
            bookGenres.Genres = genreRepo.GetAll().ToList();
            bookGenres.Book = bookRepo.GetById(id);

            return View(bookGenres);
        }

        [Route("izmena")]
        [HttpPost]
        public ActionResult Edit(BookGenreViewModel bookGenres)
        {
            try
            {
                bookRepo.Update(bookGenres);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}