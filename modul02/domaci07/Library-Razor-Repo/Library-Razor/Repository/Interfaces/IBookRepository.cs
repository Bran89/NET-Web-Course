﻿using Library_Razor.Models;
using Library_Razor.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library_Razor.Repository.Interfaces
{
    public interface IBookRepository
    {
        IEnumerable<Book> GetAll();
        Book GetById(int id);
        bool Create(BookGenreViewModel bookGenres);
        void Update(BookGenreViewModel bookGenres);
        void Delete(int id);
        bool Exist(string name);
        IEnumerable<Book> FindByTitle(string name);
    }
}
