﻿using Library_Razor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library_Razor.Repository.Interfaces
{
    interface IGenreRepository
    {
        IEnumerable<Genre> GetAll();
        Genre GetById(int id);
        bool Create(Genre book);
        void Update(Genre book);
        void Delete(int id);
    }
}
