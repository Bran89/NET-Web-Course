﻿using Library_Razor.Models;
using Library_Razor.Repository.Interfaces;
using Library_Razor.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Library_Razor.Repository
{
    public class BookRepo : IBookRepository
    {
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AlephDbContext"].ToString();
            con = new SqlConnection(constr);
        }
        public bool Create(BookGenreViewModel bookGenres)
        {
            try
            {
                string query = "INSERT INTO Book (BookName, BookPrice, GenreId, BookDeleted) VALUES (@BookName, @BookPrice, @GenreId, @BookDeleted);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookName", bookGenres.Book.Name);
                    cmd.Parameters.AddWithValue("@BookPrice", bookGenres.Book.Price);
                    cmd.Parameters.AddWithValue("@GenreId", bookGenres.SelectedGenreId);
                    cmd.Parameters.AddWithValue("@BookDeleted", 0);

                    con.Open();
                    var newFormedId = cmd.ExecuteScalar();
                    con.Close();

                    if (newFormedId != null)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom upisa nove knjige. " + ex.StackTrace);
                throw ex;
            }
        }

        public void Delete(int id)
        {
            try
            {
                string query = "UPDATE Book SET BookDeleted = '1' WHERE BookId = @BookId;";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookId", id);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja knjige. {ex.StackTrace}");
                throw ex;
            }
        }

        public IEnumerable<Book> GetAll()
        {
            string query = "SELECT * FROM Book LEFT JOIN Genre ON Book.GenreId = Genre.GenreId;";
            Connection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;
                con.Open();

                dadapter.Fill(ds, "Book");
                dt = ds.Tables["Book"];
                con.Close();
            }

            List<Book> books = new List<Book>();

            foreach (DataRow dataRow in dt.Rows)
            {
                int bookId = int.Parse(dataRow["BookId"].ToString());
                string bookName = dataRow["BookName"].ToString();
                int bookPrice = int.Parse(dataRow["BookPrice"].ToString());
                bool bookDeleted = (bool)dataRow["BookDeleted"];

                int genreId = int.Parse(dataRow["GenreId"].ToString());
                string genreName = dataRow["GenreName"].ToString();
                bool genreDeleted = (bool)dataRow["GenreDeleted"];


                Genre genre = new Genre()
                {
                    Id = genreId,
                    Name = genreName,
                    Deleted = genreDeleted
                };

                books.Add(new Book() { Id = bookId, Name = bookName, Price = bookPrice, Genre = genre, Deleted = bookDeleted });
            }

            return books;
        }

        public Book GetById(int id)
        {
            Book book = null;

            try
            {
                string query = "SELECT * FROM Book b LEFT JOIN Genre ON b.GenreId = Genre.GenreId WHERE b.BookId = @BookId;";
                Connection();

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    con.Open();
                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int bookId = int.Parse(dataRow["BookId"].ToString());
                    string bookName = dataRow["BookName"].ToString();
                    int bookPrice = int.Parse(dataRow["BookPrice"].ToString());
                    bool bookDeleted = (bool)dataRow["BookDeleted"];

                    int genreId = int.Parse(dataRow["GenreId"].ToString());
                    string genreName = dataRow["GenreName"].ToString();
                    bool genreDeleted = (bool)dataRow["GenreDeleted"];

                    Genre genre = new Genre()
                    {
                        Id = genreId,
                        Name = genreName,
                        Deleted = genreDeleted
                    };

                    book = new Book() { Id = bookId, Name = bookName, Price = bookPrice, Genre = genre, Deleted = bookDeleted };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja knjige sa id-om: {book.Id}. {ex.StackTrace}");
                throw ex;
            }

            return book;
        }

        public bool Exist(string name)
        {
            try
            {
                //string query = "SELECT BookName FROM Book WHERE EXISTS (SELECT BookName FROM Book WHERE BookName = @BookName);";
                string query = "SELECT BookName FROM Book WHERE BookName = @BookName;";
                Connection();

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookName", name);

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    con.Open();
                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }

                if (dt.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja imena knjige! {ex.StackTrace}");
                throw ex;
            }
        }

        public void Update(BookGenreViewModel bookGenres)
        {
            try
            {
                string query = "UPDATE Book SET BookName = @BookName, BookPrice = @BookPrice, GenreId = @GenreId, BookDeleted = @BookDeleted WHERE BookId = @BookId;";
                
                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookName", bookGenres.Book.Name);
                    cmd.Parameters.AddWithValue("@BookPrice", bookGenres.Book.Price);
                    cmd.Parameters.AddWithValue("@GenreId", bookGenres.SelectedGenreId);
                    cmd.Parameters.AddWithValue("@BookDeleted", bookGenres.Book.Deleted);
                    cmd.Parameters.AddWithValue("@BookId", bookGenres.Book.Id);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska prilikom izmena knjige. " + ex.StackTrace);
                throw ex;
            }
        }

        public IEnumerable<Book> FindByTitle(string name)
        {
            string query = "SELECT * FROM Book LEFT JOIN Genre ON Book.GenreId = Genre.GenreId WHERE BookName LIKE @BookName;";
            Connection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            string bName = "%" + name + "%";

            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@BookName", bName);

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;

                con.Open();
                dadapter.Fill(ds, "Book");
                dt = ds.Tables["Book"];
                con.Close();
            }

            List<Book> books = new List<Book>();

            foreach (DataRow dataRow in dt.Rows)
            {
                int bookId = int.Parse(dataRow["BookId"].ToString());
                string bookName = dataRow["BookName"].ToString();
                int bookPrice = int.Parse(dataRow["BookPrice"].ToString());
                bool bookDeleted = (bool)dataRow["BookDeleted"];

                int genreId = int.Parse(dataRow["GenreId"].ToString());
                string genreName = dataRow["GenreName"].ToString();
                bool genreDeleted = (bool)dataRow["GenreDeleted"];


                Genre genre = new Genre()
                {
                    Id = genreId,
                    Name = genreName,
                    Deleted = genreDeleted
                };

                books.Add(new Book() { Id = bookId, Name = bookName, Price = bookPrice, Genre = genre, Deleted = bookDeleted });
            }

            return books;
        }
    }
}