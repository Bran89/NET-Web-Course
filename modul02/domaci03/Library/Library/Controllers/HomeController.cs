﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;

namespace Library.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<hr/><h1>Bookstore - Home page</h1><hr/>");
            sb.Append("<form method='post' action='/Book/AddBook'>");
            sb.Append("<table border=\"1\">");
            sb.Append("<tr>");
            sb.Append("<th colspan=\"2\">Add new book</th>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td><b>Name of book:</b></td>");
            sb.Append("<td><input type=\"text\" name=\"Name\"</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td><b>Price of book:</b></td>");
            sb.Append("<td><input type=\"text\" name=\"Price\"</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td><b>Genre of book:</b></td>");
            sb.Append("<td><select name=\"Genre\">");
            sb.Append("<option value=\"Science\">Science</option>");
            sb.Append("<option value=\"Comedy\">Comedy</option>");
            sb.Append("<option value=\"Horror\">Horror</option>");
            sb.Append("</select></td>");
            sb.Append("</tr>");
            sb.Append("<tr align=\"right\">");
            sb.Append("<td colspan=\"2\">");
            sb.Append("<input type='submit' value='Add'/>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<a href=\"/bookstore/ShowBooks\">Show All Books</a>&nbsp;");
            sb.Append("&verbar;&nbsp;<a href=\"/bookstore/ShowDeleted\">Show Deleted Books</a>");

            return Content(sb.ToString());
        }
    }
}