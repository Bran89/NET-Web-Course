﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class BookController : Controller
    {
        [HttpPost]
        public ActionResult AddBook(Book b)
        {
            b.Id = Book.Brojac;
            Book.Brojac++;
            Bookstore.Books.Add(b);

            return this.RedirectToAction("ShowBooks", "Bookstore");
        }

        public ActionResult Delete(int id)
        {
            foreach (Book b in Bookstore.Books)
            {
                if (b.Id == id)
                {
                    b.Deleted = true;
                    break;
                }
            }

            return this.RedirectToAction("ShowBooks", "Bookstore");
        }
    }
}   