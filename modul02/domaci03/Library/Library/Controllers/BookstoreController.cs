﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class BookstoreController : Controller
    {
        // GET: Bookstore
        public ActionResult ShowBooks()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<h1>Showing all books</h1><hr/>");
            sb.Append("<table border=\"1\"");
            sb.Append("<tr>");
            sb.Append("<th>Id</th>");
            sb.Append("<th>Name</th>");
            sb.Append("<th>Price</th>");
            sb.Append("<th>Genre</th>");
            sb.Append("</tr>");

            foreach (Book b in Bookstore.Books)
            {
                if (b.Deleted == false)
                {
                    sb.Append("<tr>");
                    sb.Append($"<td> { b.Id }</td>");
                    sb.Append($"<td> { b.Name }</td>");
                    sb.Append($"<td> { b.Price }</td>");
                    sb.Append($"<td> { b.Genre }</td>");
                    sb.Append($"<td><a href=\"/book/delete?id={ b.Id }\">delete</a></td>");
                    sb.Append("</tr>");
                }
            }

            sb.Append("</table>");
            sb.Append("<a href=\"/home/index\">Link to homepage</a>");

            return Content(sb.ToString());
        }

        public ActionResult ShowDeleted()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<h1>Showing deleted books</h1><hr/>");
            sb.Append("<table border=\"1\"");
            sb.Append("<tr>");
            sb.Append("<th>Id</th>");
            sb.Append("<th>Name</th>");
            sb.Append("<th>Price</th>");
            sb.Append("<th>Genre</th>");
            sb.Append("</tr>");

            foreach (Book b in Bookstore.Books)
            {
                if (b.Deleted)
                {
                    sb.Append("<tr>");
                    sb.Append($"<td> { b.Id }</td>");
                    sb.Append($"<td> { b.Name }</td>");
                    sb.Append($"<td> { b.Price }</td>");
                    sb.Append($"<td> { b.Genre }</td>");
                    sb.Append("</tr>");
                }
            }

            sb.Append("</table>");
            sb.Append("<a href=\"/home/index\">Link to homepage</a>");

            return Content(sb.ToString());
        }
    }
}