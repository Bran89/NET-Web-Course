﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.Models
{
    public class Bookstore
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public static List<Book> Books = new List<Book>();

        public Bookstore()
        {
            Books = new List<Book>();
        }
    }
}