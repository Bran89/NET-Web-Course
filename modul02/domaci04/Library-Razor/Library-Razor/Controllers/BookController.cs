﻿using Library_Razor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library_Razor.Controllers
{
    public class BookController : Controller
    {
        // GET: Book
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Book b)
        {
            b.Id = Book.Brojac;
            Book.Brojac++;
            Bookstore.Books.Add(b);

            return this.RedirectToAction("Index", "Bookstore");
        }

        public ActionResult Delete(int id)
        {
            foreach (Book b in Bookstore.Books)
            {
                if (b.Id == id)
                {
                    b.Deleted = true;
                    break;
                }
            }

            return this.RedirectToAction("Index", "Bookstore");
        }
    }
}