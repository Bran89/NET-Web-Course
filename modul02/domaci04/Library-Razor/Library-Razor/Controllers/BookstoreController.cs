﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library_Razor.Controllers
{
    public class BookstoreController : Controller
    {
        // GET: Bookstore
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowBooks()
        {
            return RedirectToAction("Index");
        }

        public ActionResult Delete()
        {
            return View();
        }
    }
}