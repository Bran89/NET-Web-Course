CREATE TABLE [dbo].[Car]
(
	[CarId] INT IDENTITY (1,1) PRIMARY KEY,
	[CarBrand] NVARCHAR(80) NULL,
	[CarModel] NVARCHAR(80) NULL,
	[CarYear] INT NULL,
	[CarColor] NVARCHAR(80) NULL,
	[CarDisplacement] INT NULL,
	[EngineId] INT NULL,
	FOREIGN KEY (EngineId) REFERENCES dbo.Engine(EngineId)
)