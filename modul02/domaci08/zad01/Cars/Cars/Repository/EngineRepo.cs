﻿using Cars.Models;
using Cars.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Cars.Repository
{
    public class EngineRepo : IEngineRepository
    {
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AlephDbContext"].ToString();
            con = new SqlConnection(constr);
        }
        public bool Create(Engine engine)
        {
            try
            {
                string query = "INSERT INTO Engine (EngineName) VALUES (@EngineName);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@EngineName", engine.Name);

                    con.Open();
                    var newFormedId = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();

                    if (newFormedId != null)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska prilikom upisa novog motora. " + ex.StackTrace);
                throw ex;
            }
        }

        public void Delete(int id)
        {
            try
            {
                string query = "DELETE FROM Engine WHERE EngineId = @EngineId;";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@EngineId", id);

                    con.Open();
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja motora. {ex.StackTrace}");
                throw ex;
            }
        }

        public IEnumerable<Engine> GetAll()
        {
            string query = "SELECT * FROM Engine;";
            Connection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;
                con.Open();

                dadapter.Fill(ds, "Genre");
                dt = ds.Tables["Genre"];
                con.Close();
            }

            List<Engine> engines = new List<Engine>();

            foreach (DataRow dataRow in dt.Rows)
            {
                int engineId = int.Parse(dataRow["EngineId"].ToString());
                string engineName = dataRow["EngineName"].ToString();

                engines.Add(new Engine() { Id = engineId, Name = engineName });
            }

            return engines;
        }

        public Engine GetById(int id)
        {
            Engine engine = null;

            try
            {
                string query = "SELECT * FROM Engine pc WHERE pc.EngineId = @EngineId;";
                Connection();

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@EngineId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    con.Open();
                    dadapter.Fill(ds, "Engine");
                    dt = ds.Tables["Engine"];
                    con.Close();
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int engineId = int.Parse(dataRow["EngineId"].ToString());
                    string engineName = dataRow["EngineName"].ToString();

                    engine = new Engine() { Id = engineId, Name = engineName };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja motora sa id-om: {engine.Id}. {ex.StackTrace}");
                throw ex;
            }

            return engine;
        }

        public void Update(Engine engine)
        {
            try
            {
                string query = "UPDATE Engine SET EngineName = @EngineName WHERE EngineId = @EngineId;";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@EngineId", engine.Id);
                    cmd.Parameters.AddWithValue("@EngineName", engine.Name);

                    con.Open();
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom menjanja podataka motora. {ex.StackTrace}");
                throw ex;
            }
        }
    }
}