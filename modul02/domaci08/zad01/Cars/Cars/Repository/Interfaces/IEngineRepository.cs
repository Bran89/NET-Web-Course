﻿using Cars.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cars.Repository.Interfaces
{
    public interface IEngineRepository
    {
        IEnumerable<Engine> GetAll();
        Engine GetById(int id);
        bool Create(Engine engine);
        void Update(Engine engine);
        void Delete(int id);
    }
}
