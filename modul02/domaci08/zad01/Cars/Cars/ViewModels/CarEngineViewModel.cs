﻿using Cars.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cars.ViewModels
{
    public class CarEngineViewModel
    {
        public Car Car { get; set; }
        public List<Engine> Engines { get; set; }
    }
}