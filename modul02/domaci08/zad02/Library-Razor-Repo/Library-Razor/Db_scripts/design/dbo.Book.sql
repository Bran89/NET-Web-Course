CREATE TABLE [dbo].[Book]
(
	[BookId] INT IDENTITY (1,1) PRIMARY KEY,
	[BookName] NVARCHAR (80) NULL,
	[BookPrice] NUMERIC NULL,
	[GenreId] INT NULL,
	[BookDeleted] BIT NOT NULL,
	FOREIGN KEY (GenreId) REFERENCES dbo.Genre(GenreId)
)