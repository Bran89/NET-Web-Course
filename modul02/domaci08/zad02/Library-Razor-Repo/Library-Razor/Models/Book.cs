﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library_Razor.Models
{
    public class Book
    {
        public int Id { get; set; }
        [Required]
        [StringLength(40, MinimumLength = 1)]
        public string Name { get; set; }
        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Uneli ste neispravnu cenu!")]
        public int Price { get; set; }
        public Genre Genre { get; set; }
        public bool Deleted { get; set; }

        public Book()
        {
            this.Deleted = false;
        }
    }
}