﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library_Razor.Models
{
    public class Bookstore
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}