﻿using Library_Razor.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library_Razor.ViewModels
{
    public class BookGenreViewModel
    {
        public Book Book { get; set; }
        public List<Genre> Genres { get; set; }
        [Required]
        public int SelectedGenreId { get; set; }
    }
}