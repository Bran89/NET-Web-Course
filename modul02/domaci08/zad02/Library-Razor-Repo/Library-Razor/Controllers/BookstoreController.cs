﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library_Razor.Models;
using Library_Razor.Repository;
using Library_Razor.Repository.Interfaces;

namespace Library_Razor.Controllers
{
    [RoutePrefix("Knjizara")]
    public class BookstoreController : Controller
    {
        private IBookRepository bookRepo = new BookRepo();
        
        [Route("")]
        public ActionResult List()
        {
            var books = bookRepo.GetAll();
            return View(books);
        }

        [Route("izbrisane")]
        public ActionResult Deleted()
        {
            var books = bookRepo.GetAll();
            return View(books);
        }
    }
}