﻿using Library_Razor.Models;
using Library_Razor.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Library_Razor.Repository
{
    public class GenreRepo : IGenreRepository
    {
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AlephDbContext"].ToString();
            con = new SqlConnection(constr);
        }
        public bool Create(Genre genre)
        {
            try
            {
                string query = "INSERT INTO Genre (GenreName, GenreDeleted) VALUES (@GenreName, 0);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreName", genre.Name);

                    con.Open();
                    var newFormedId = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();

                    if (newFormedId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;   // upis bezuspesan
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska prilikom upisa novog zanra. " + ex.StackTrace);
                throw ex;
            }
        }

        public void Delete(int id)
        {
            try
            {
                string query = "UPDATE Genre SET GenreDeleted = '1' WHERE GenreId = @GenreId;";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreId", id);

                    con.Open();
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja zanra. {ex.StackTrace}");
                throw ex;
            }
        }

        public IEnumerable<Genre> GetAll()
        {
            string query = "SELECT * FROM Genre;";
            Connection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;
                con.Open();

                dadapter.Fill(ds, "Genre");
                dt = ds.Tables["Genre"];
                con.Close();
            }

            List<Genre> genres = new List<Genre>();

            foreach (DataRow dataRow in dt.Rows)
            {
                int genreId = int.Parse(dataRow["GenreId"].ToString());
                string genreName = dataRow["GenreName"].ToString();
                bool genreDeleted = (bool)dataRow["GenreDeleted"];

                genres.Add(new Genre() { Id = genreId, Name = genreName, Deleted = genreDeleted });
            }

            return genres;
        }

        public Genre GetById(int id)
        {
            Genre genre = null;

            try
            {
                string query = "SELECT * FROM Genre g WHERE g.GenreId = @GenreId;";
                Connection();

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    con.Open();
                    dadapter.Fill(ds, "Genre");
                    dt = ds.Tables["Genre"];
                    con.Close();
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int genreId = int.Parse(dataRow["GenreId"].ToString());
                    string genreName = dataRow["GenreName"].ToString();
                    bool genreDeleted = (bool)dataRow["GenreDeleted"];

                    genre = new Genre() { Id = genreId, Name = genreName, Deleted = genreDeleted };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja zanra sa id-om: {genre.Id}. {ex.StackTrace}");
                throw ex;
            }

            return genre;
        }

        public void Update(Genre genre)
        {
            try
            {
                string query = "UPDATE Genre SET GenreName = @GenreName WHERE GenreId = @GenreId;";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreId", genre.Id);
                    cmd.Parameters.AddWithValue("@GenreName", genre.Name);

                    con.Open();
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izmena zanra. {ex.StackTrace}");
                throw ex;
            }
        }
    }
}