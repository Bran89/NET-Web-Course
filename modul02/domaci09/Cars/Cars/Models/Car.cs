﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cars.Models
{
    public class Car
    {
        public int Id { get; set; }
        [Required]
        [StringLength(40, MinimumLength = 2)]
        public string Brand { get; set; }
        [Required]
        [StringLength(40, MinimumLength = 1)]
        public string Model { get; set; }
        [Required]
        [Range(1900, 2019, ErrorMessage ="{0} must be between 1900 and 2019")]
        public int Year { get; set; }
        [Required]
        [StringLength(40, MinimumLength = 3)]
        public string Color { get; set; }
        [Range(1, 10000, ErrorMessage = "{0} must be between 1 and 10000")]
        public int Displacement { get; set; }
        public Engine Engine { get; set; }
        //public int EngineId { get; set; }
    }
}