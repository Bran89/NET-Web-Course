﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cars.Models
{
    public class Engine
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="blabla")]
        [StringLength(40, MinimumLength = 1)]
        [Display(Name = "Engine Type")]
        public string Name { get; set; }
    }
}