﻿using Cars.Models;
using Cars.Repository;
using Cars.Repository.Interfaces;
using Cars.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cars.Controllers
{
    public class CarController : Controller
    {
        private ICarRepository carRepo = new CarRepo();
        private IEngineRepository engineRepo = new EngineRepo();
        // GET: Car
        public ActionResult Index()
        {
            var cars = carRepo.GetAll();
            return View(cars);
        }

        public ActionResult Create()
        {
            CarEngineViewModel carEngines = new CarEngineViewModel();
            carEngines.Engines = engineRepo.GetAll().ToList();

            return View(carEngines);
        }

        [HttpPost]
        public ActionResult Create(Car car) //[Bind(Exclude = "Engine")]Car car)
        {
            ModelState.Values.ElementAt(6).Errors.Clear();
            if (!ModelState.IsValid)
            {
                CarEngineViewModel carEngines = new CarEngineViewModel();
                carEngines.Car = car;
                carEngines.Engines = engineRepo.GetAll().ToList();
                return View(carEngines);
            }
            if (carRepo.Create(car))
            {
                return RedirectToAction("Index");
            }

            return View("Error");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                carRepo.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            CarEngineViewModel carEngines = new CarEngineViewModel();
            carEngines.Engines = engineRepo.GetAll().ToList();
            carEngines.Car = carRepo.GetById(id);

            return View(carEngines);
        }

        [HttpPost]
        public ActionResult Edit(int id, Car car)
        {
            try
            {
                Engine engine = new Engine() { Id = id };
                car.Engine = engine;

                carRepo.Update(car);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}