﻿using Cars.Models;
using Cars.Repository;
using Cars.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cars.Controllers
{
    public class EngineController : Controller
    {
        private IEngineRepository engineRepo = new EngineRepo();
        // GET: Engine
        public ActionResult Index()
        {
            var engines = engineRepo.GetAll();

            return View(engines);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Engine engine)
        {
            if (!ModelState.IsValid)
            {
                return View(engine);
            }
            if (engineRepo.Create(engine))
            {
                return RedirectToAction("Index");
            }

            return View("Error");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                engineRepo.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            Engine engine = new Engine();
            engine = engineRepo.GetById(id);

            return View(engine);
        }

        [HttpPost]
        public ActionResult Edit(int id, Engine engine)
        {
            try
            {
                engineRepo.Update(engine);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}