﻿using Cars.Models;
using Cars.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Cars.Repository
{
    public class CarRepo : ICarRepository
    {
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AlephDbContext"].ToString();
            con = new SqlConnection(constr);
        }
        public bool Create(Car car)
        {
            try
            {
                string query = "INSERT INTO Car (CarBrand, CarModel, CarYear, CarColor, CarDisplacement, EngineId) VALUES (@CarBrand, @CarModel, @CarYear, @CarColor, @CarDisplacement, @EngineId);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@CarBrand", car.Brand);
                    cmd.Parameters.AddWithValue("@CarModel", car.Model);
                    cmd.Parameters.AddWithValue("@CarYear", car.Year);
                    cmd.Parameters.AddWithValue("@CarColor", car.Color);
                    cmd.Parameters.AddWithValue("@CarDisplacement", car.Displacement);
                    cmd.Parameters.AddWithValue("@EngineId", car.Engine.Id);

                    con.Open();
                    var newFormedId = cmd.ExecuteScalar();
                    con.Close();

                    if (newFormedId != null)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska prilikom upisa novog automobila. " + ex.StackTrace);
                throw ex;
            }
        }

        public void Delete(int id)
        {
            try
            {
                string query = "DELETE FROM Car WHERE CarId = @CarId;";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@CarId", id);

                    con.Open();
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja automobila. {ex.StackTrace}");
                throw ex;
            }
        }

        public IEnumerable<Car> GetAll()
        {
            string query = "SELECT * FROM Car LEFT JOIN Engine ON Car.EngineId = Engine.EngineId;";
            Connection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;
                con.Open();

                dadapter.Fill(ds, "Book");
                dt = ds.Tables["Book"];
                con.Close();
            }

            List<Car> cars = new List<Car>();

            foreach (DataRow dataRow in dt.Rows)
            {
                int carId = int.Parse(dataRow["CarId"].ToString());
                string carBrand = dataRow["CarBrand"].ToString();
                string carModel = dataRow["CarModel"].ToString();
                int carYear = int.Parse(dataRow["CarYear"].ToString());
                string carColor = dataRow["CarColor"].ToString();
                int carDisplacement = int.Parse(dataRow["CarDisplacement"].ToString());

                int engineId = int.Parse(dataRow["EngineId"].ToString());
                string engineName = dataRow["EngineName"].ToString();


                Engine engine = new Engine()
                {
                    Id = engineId,
                    Name = engineName
                };

                cars.Add(new Car() { Id = carId, Brand = carBrand, Model = carModel, Year = carYear, Color = carColor, Displacement = carDisplacement, Engine = engine });
            }

            return cars;
        }

        public Car GetById(int id)
        {
            Car car = null;

            try
            {
                string query = "SELECT * FROM Car c LEFT JOIN Engine ON c.EngineId = Engine.EngineId WHERE c.CarId = @CarId;";
                Connection();

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@CarId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    con.Open();
                    dadapter.Fill(ds, "Car");
                    dt = ds.Tables["Car"];
                    con.Close();
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int carId = int.Parse(dataRow["CarId"].ToString());
                    string carBrand = dataRow["CarBrand"].ToString();
                    string carModel = dataRow["CarModel"].ToString();
                    int carYear = int.Parse(dataRow["CarYear"].ToString());
                    string carColor = dataRow["CarColor"].ToString();
                    int carDisplacement = int.Parse(dataRow["CarDisplacement"].ToString());

                    int engineId = int.Parse(dataRow["EngineId"].ToString());
                    string engineName = dataRow["EngineName"].ToString();

                    Engine engine = new Engine()
                    {
                        Id = engineId,
                        Name = engineName,
                    };

                    car = new Car() { Id = carId, Brand = carBrand, Model = carModel, Year = carYear, Color = carColor, Displacement = carDisplacement, Engine = engine };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja knjige sa id-om: {car.Id}. {ex.StackTrace}");
                throw ex;
            }

            return car;
        }

        public void Update(Car car)
        {
            try
            {
                string query = "UPDATE Car SET CarBrand = @CarBrand, CarModel = @CarModel, CarYear = @CarYear, CarColor = @CarColor, CarDisplacement = @CarDisplacement, EngineId = @EngineId WHERE CarId = @CarId;";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@CarBrand", car.Brand);
                    cmd.Parameters.AddWithValue("@CarModel", car.Model);
                    cmd.Parameters.AddWithValue("@CarYear", car.Year);
                    cmd.Parameters.AddWithValue("@CarColor", car.Color);
                    cmd.Parameters.AddWithValue("@CarDisplacement", car.Displacement);
                    cmd.Parameters.AddWithValue("@EngineId", car.Engine.Id);
                    cmd.Parameters.AddWithValue("@CarId", car.Id);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska prilikom izmena automobila. " + ex.StackTrace);
                throw ex;
            }
        }
    }
}